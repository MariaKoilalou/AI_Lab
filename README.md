# AI_Lab
This repository contains all of the lab assignments for the Artificial Intelligence course of Maria Koilalou and Justine Le Bourg. 
Each lab assignment is organized in a separate folder containing the necessary files.


## AI_Lab 1

The `AI_Lab1` folder contains the files for our first lab assignment, which includes:

- A report in PDF format describing the methodology, results, and conclusions of our project. See [AI_Lab1-report.pdf](AI_Lab1/AI_Lab1-report.pdf).
- A Python script named `variant1.py` which implements the machine learning model discussed in the report. See [variant1.py](AI_Lab1/variant1.py).
- A Textile file named `maze.txt` containing the documentation and instructions for running the Python script. See [maze.txt](AI_Lab1/maze.txt).


## AI_Lab 2

The `AI_Lab2` folder contains the files for our first lab assignment, which includes:

- A report in PDF format describing the methodology, results, and conclusions of our project. See [AI_Lab2-report.pdf](AI_Lab2/AI_Lab2-report.pdf).
- A Python script named `variant1.py` which implements the machine learning model discussed in the report. See [variant1.py](AI_Lab2/variant1.py).


## AI_Lab 3

The `AI_Lab3` folder contains the files for our first lab assignment, which includes:

- A report in PDF format describing the methodology, results, and conclusions of our project. See [AI_Lab3-report.pdf](AI_Lab3/AI_Lab3-report.pdf).
- A Python script named `variant1.py` which implements the machine learning model discussed in the report. See [variant1.py](AI_Lab3/variant1.py).


## AI_Lab 4

The `AI_Lab4` folder contains the files for our first lab assignment, which includes:

- A report in PDF format describing the methodology, results, and conclusions of our project. See [AI_Lab4-report.pdf](AI_Lab4/AI_Lab4-report.pdf).
- A Python script named `variant1.py` which implements the machine learning model discussed in the report. See [variant1.py](AI_Lab4/variant1.py).
- A .csv file with the data used in our code. See [variant1.csv](AI_Lab4/variant1.csv)

